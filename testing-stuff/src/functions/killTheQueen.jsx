import React from "react";
import MaterialIcon, { colorPalette } from "material-icons-react";

export class KillTheQueen extends React.Component {
  render() {
    return (
      <span
        className="project-icons"
        onClick={() => {
          const element = document.getElementById("burkContainer");
          element.style.opacity = "0";
          setTimeout(function() {
            element.parentNode.removeChild(element);
          }, 1000);
        }}
      >
        <MaterialIcon icon="close" />
      </span>
    );
  }
}
